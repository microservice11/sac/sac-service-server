
from django.urls import include, path
from rest_framework import routers

from config_service.views import CheckViews, CreateViews, CheckUnregSSH, CheckRegSSH, CheckServiceSSH, BatchViews

app_name = 'config_service'
router = routers.DefaultRouter()
router.register(prefix='config/batch', viewset=BatchViews, basename='batch')
router.register(prefix='config/check/unreg', viewset=CheckViews, basename='check_unreg')
router.register(prefix='config/check/unreg/ssh-hw', viewset=CheckUnregSSH, basename='check_unreg_hw')
router.register(prefix='config/check/reg', viewset=CheckViews, basename='check_reg')
router.register(prefix='config/check/reg/ssh-hw', viewset=CheckRegSSH, basename='check_reg_hw')
router.register(prefix='config/check/service', viewset=CheckViews, basename='check_service')
router.register(prefix='config/check/service/ssh-hw', viewset=CheckServiceSSH, basename='check_service_hw')
router.register(prefix='config/create', viewset=CreateViews, basename='config')

urlpatterns = [
    path('', include(router.urls))
]
