import pika


class RabbitMQ:

    def __init__(self) -> None:
        connect = pika.BlockingConnection(
            pika.ConnectionParameters(host='rabbitmq',
                                      port=5672,
                                      credentials=pika.credentials.PlainCredentials('guest', 'guest')))
        self.__channel = connect.channel()

    def channel(self):
        return self.__channel

    def close(self):
        self.__channel.close()


connection = RabbitMQ()
channel = connection.channel()

EXCHANGE_LOGS = 'logs'
ROUTING_DATA_CONFIG = 'data.config'
ROUTING_DATA_CONFIG_DETAIL = 'data.configDetail'
ROUTING_DATA_DEAD_LETTER = 'data.deadLetter'
QUEUE_LOGS_DATA_CONFIG = 'logs.data.config'
QUEUE_LOGS_DATA_CONFIG_DETAIL = 'logs.data.configDetail'
QUEUE_LOGS_DATA_DEAD_LETTER = 'logs.data.deadLetter'

channel.queue_declare(queue=QUEUE_LOGS_DATA_DEAD_LETTER, durable=True)
channel.exchange_declare(exchange=EXCHANGE_LOGS, exchange_type='topic', durable=True)
channel.queue_bind(queue=QUEUE_LOGS_DATA_DEAD_LETTER, exchange=EXCHANGE_LOGS, routing_key=ROUTING_DATA_DEAD_LETTER)

channel.queue_declare(queue=QUEUE_LOGS_DATA_CONFIG, durable=True,
                      arguments={'x-dead-letter-exchange': EXCHANGE_LOGS,
                                 'x-dead-letter-routing-key': ROUTING_DATA_DEAD_LETTER})
channel.exchange_declare(exchange=EXCHANGE_LOGS, exchange_type='topic', durable=True)
channel.queue_bind(queue=QUEUE_LOGS_DATA_CONFIG, exchange=EXCHANGE_LOGS, routing_key=ROUTING_DATA_CONFIG)

channel.queue_declare(queue=QUEUE_LOGS_DATA_CONFIG_DETAIL, durable=True,
                      arguments={'x-dead-letter-exchange': EXCHANGE_LOGS,
                                 'x-dead-letter-routing-key': ROUTING_DATA_DEAD_LETTER})
channel.exchange_declare(exchange=EXCHANGE_LOGS, exchange_type='topic', durable=True)
channel.queue_bind(queue=QUEUE_LOGS_DATA_CONFIG_DETAIL, exchange=EXCHANGE_LOGS, routing_key=ROUTING_DATA_CONFIG_DETAIL)
channel.close()
