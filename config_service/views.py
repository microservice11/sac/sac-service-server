import base64
import time
from datetime import datetime

import paramiko
from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.utils import json

from config_service import RabbitMQ
from config_service.utils.CmdGpon import CmdGpon
from config_service.utils.TL1Socket import TL1Socket


def send_log_config_check(message, status='SUCCESS'):
    message['data']['status'] = status
    message['operation'] = 'update'
    rabbit = RabbitMQ()
    channel = rabbit.channel()
    payload = json.dumps(message)
    channel.basic_publish(exchange='logs', routing_key='data.config', body=payload)
    # print(f" [x] Sent logs.config:  % {payload}")
    channel.close()


def send_log_detail_config_check(message):
    rabbit = RabbitMQ()
    channel = rabbit.channel()
    payload = json.dumps(message)
    channel.basic_publish(exchange='logs', routing_key='data.configDetail', body=payload)
    # print(f" [x] Sent logs.config.detail:  % {payload}")
    channel.close()


def get_log(request, action, target=None, proccess_id=None, type=0):
    payload = request.data
    if isinstance(payload, (list, tuple)):
        target_list = []
        for data in payload:
            target_list.append(data['nms']['ip_server'])

        target = f"IP NMS: {','.join(target_list)}"
    username = request.headers['Username']
    ip_client = request.headers['Ip-Client']
    now = datetime.now()
    dt_obj = datetime.strptime(now.strftime("%d.%m.%Y %H:%M:%S,%f"), "%d.%m.%Y %H:%M:%S,%f")
    log_id = dt_obj.timestamp() * 1000000 if proccess_id is None else proccess_id
    param_data = {
        'operation': 'insert',
        'data': {
            'id': log_id,
            'target': target,
            'action': action,
            'status': 'IN_PROGRESS',
            'ip_client': ip_client,
            'username': username,
            'type': type,
        }
    }
    rabbit = RabbitMQ()
    channel = rabbit.channel()
    payloads = json.dumps(param_data)
    channel.basic_publish(exchange='logs', routing_key='data.config', body=payloads)
    # print(f" [x] Sent logs.config:  % {payloads}")
    channel.close()
    return param_data


def get_log_detail(payload, target, result):
    payload['target'] = target
    # print(result['command'])
    payload['cmd'] = result['command']
    payload['result'] = result['result']
    # print(payload)
    send_log_detail_config_check(payload)


class CheckUnregSSH(viewsets.ViewSet):

    def create(self, request):
        log_data = None
        data_list = request.data
        result_list = []
        cmd_gpon = CmdGpon()
        for data in data_list:
            ip_server = data['nms']['ip_server']
            port_tl1 = data['nms']['port_tl1']
            username = data['nms']['username']
            password = data['nms']['password']
            gpons = data['gpon']
            ssh_client = paramiko.SSHClient()
            ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

            if log_data is None:
                log_data = get_log(request, 'CEK SN UNREG/REG/SERVICE')

            message = {
                'target': None,
                'action': 'CEK UNREGISTER ONT',
                'status': 'IN_PROGRESS',
                'cmd': 'display ont autofind all',
                'result': None,
                'username': request.headers['Username'],
                'log_config_id': log_data['data']['id'],
            }

            try:
                ssh_client.connect(hostname=ip_server, port=port_tl1, username=base64.b64decode(username).decode(),
                                   password=base64.b64decode(password).decode(), look_for_keys=False)
            except Exception as e:
                hasil = f"timeout for connecting {ip_server} port {port_tl1}"
                message['target'] = f"IP NMS: {ip_server}"
                message['result'] = hasil
                message['status'] = 'FAILED'
                send_log_detail_config_check(message)
                send_log_config_check(log_data, 'FAILED')
                print(hasil)
                raise Exception(f"hasil : {e}")

            for gpon in gpons:
                command = f"telnet {gpon['ip_gpon']}"
                print(command)
                stdin, stdout, stderr = ssh_client.exec_command(command)
                time.sleep(2)
                if gpon['username'] is not None or gpon['password'] is not None:
                    username = gpon['username']
                    password = gpon['password']
                cmd = cmd_gpon.command(username=username, password=password, type="UNREG")
                stdin.write(cmd)
                hasil = stdout.read().decode()
                print(hasil)
                result_list.append(hasil)
                message['target'] = f"IP GPON: {gpon['ip_gpon']}"
                message['result'] = hasil
                message['status'] = 'SUCCESS'
                send_log_detail_config_check(message)

            ssh_client.exec_command("logout")
            ssh_client.close()

        send_log_config_check(log_data)
        data = {
            "status": "SUCCESS",
            "data": data_list,
            "result": result_list
        }
        return Response(data)


class CheckRegSSH(viewsets.ViewSet):

    def create(self, request):
        log_data = None
        data_list = request.data
        result_list = []
        cmd_gpon = CmdGpon()
        for data in data_list:
            ip_server = data['nms']['ip_server']
            port_tl1 = data['nms']['port_tl1']
            username = data['nms']['username']
            password = data['nms']['password']
            gpons = data['gpon']
            ssh_client = paramiko.SSHClient()
            ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

            if log_data is None:
                log_data = get_log(request, 'CEK SN UNREG/REG/SERVICE')

            message = {
                'target': None,
                'action': 'CEK REGISTER ONT',
                'status': 'IN_PROGRESS',
                'cmd': None,
                'result': None,
                'username': request.headers['Username'],
                'log_config_id': log_data['data']['id'],
            }

            try:
                ssh_client.connect(hostname=ip_server, port=port_tl1, username=base64.b64decode(username).decode(),
                                   password=base64.b64decode(password).decode(), look_for_keys=False)
            except Exception as e:
                hasil = f"timeout for connecting {ip_server} port {port_tl1}"
                message['target'] = f"IP NMS: {ip_server}"
                message['result'] = hasil
                message['status'] = 'FAILED'
                send_log_detail_config_check(message)
                send_log_config_check(log_data, 'FAILED')
                print(hasil)
                raise Exception(f"hasil : {e}")

            for gpon in gpons:
                command = f"telnet {gpon['ip_gpon']}"
                print(command)
                stdin, stdout, stderr = ssh_client.exec_command(command)
                time.sleep(2)
                slot = gpon['slot_port'].split("-")[0] if 'slot_port' in gpon else None
                port = gpon['slot_port'].split("-")[1] if 'slot_port' in gpon else None
                sn = gpon['onu_id'] if 'onu_id' in gpon else None
                if gpon['username'] is not None or gpon['password'] is not None:
                    username = gpon['username']
                    password = gpon['password']
                if sn is not None and len(sn) > 2:
                    message['target'] = f"IP GPON: {gpon['ip_gpon']}, SN: {sn}"
                    message['cmd'] = f"display ont info by-sn {sn}"
                    cmd = cmd_gpon.command(username=username, password=password, type="REG", sn=sn)
                else:
                    onu_id = gpon['onu_id'] if 'onu_id' in gpon else None
                    if onu_id is not None:
                        message['target'] = f"IP GPON: {gpon['ip_gpon']}, SLOT: {slot}, PORT: {port}, ONUID: {onu_id}"
                        message['cmd'] = f"display ont info 0 {slot} {port} {onu_id}"
                        cmd = cmd_gpon.command(username=username, password=password, type="REG", slot=slot, port=port,
                                               onu_id=onu_id)
                    else:
                        message['target'] = f"IP GPON: {gpon['ip_gpon']}, SLOT: {slot}, PORT: {port}"
                        message['cmd'] = f"display ont info 0 {slot} {port} all"
                        cmd = cmd_gpon.command(username=username, password=password, type="REG", slot=slot, port=port)

                stdin.write(cmd)
                hasil = stdout.read().decode()
                print(hasil)
                result_list.append(hasil)
                message['result'] = hasil
                message['status'] = 'SUCCESS'
                send_log_detail_config_check(message)

            ssh_client.exec_command("logout")
            ssh_client.close()

        send_log_config_check(log_data)
        data = {
            "status": "SUCCESS",
            "data": data_list,
            "result": result_list
        }
        return Response(data)


class CheckServiceSSH(viewsets.ViewSet):

    def create(self, request):
        log_data = None
        data_list = request.data
        result_list = []
        cmd_gpon = CmdGpon()
        for data in data_list:
            ip_server = data['nms']['ip_server']
            port_tl1 = data['nms']['port_tl1']
            username = data['nms']['username']
            password = data['nms']['password']
            gpons = data['gpon']
            ssh_client = paramiko.SSHClient()
            ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

            if log_data is None:
                log_data = get_log(request, 'CEK SN UNREG/REG/SERVICE')

            message = {
                'target': None,
                'action': 'CEK SERVICE ONT',
                'status': 'IN_PROGRESS',
                'cmd': None,
                'result': None,
                'username': request.headers['Username'],
                'log_config_id': log_data['data']['id'],
            }

            try:
                ssh_client.connect(hostname=ip_server, port=port_tl1, username=base64.b64decode(username).decode(),
                                   password=base64.b64decode(password).decode(), look_for_keys=False)
            except Exception as e:
                hasil = f"timeout for connecting {ip_server} port {port_tl1}"
                message['target'] = f"IP NMS: {ip_server}"
                message['result'] = hasil
                message['status'] = 'FAILED'
                send_log_detail_config_check(message)
                send_log_config_check(log_data, 'FAILED')
                print(hasil)
                raise Exception(f"hasil : {e}")

            for gpon in gpons:
                command = f"telnet {gpon['ip_gpon']}"
                print(command)
                stdin, stdout, stderr = ssh_client.exec_command(command)
                time.sleep(2)
                slot = gpon['slot_port'].split("-")[0] if 'slot_port' in gpon else None
                port = gpon['slot_port'].split("-")[1] if 'slot_port' in gpon else None
                if gpon['username'] is not None or gpon['password'] is not None:
                    username = gpon['username']
                    password = gpon['password']
                if slot is not None or port is not None:
                    onu_id = gpon['onu_id'] if 'onu_id' in gpon else None
                    if onu_id is not None:
                        message['target'] = f"IP GPON: {gpon['ip_gpon']}, SLOT: {slot}, PORT: {port}, ONUID: {onu_id}"
                        message['cmd'] = f"display service-port port 0/{slot}/{port} ont {onu_id},display ont wan-info 0/{slot} {port} {onu_id},interface gpon 0/{slot},display ont port state {port} {onu_id} pots-port all"
                        cmd = cmd_gpon.command(username=username, password=password, type="SERVICE", slot=slot,
                                               port=port,
                                               onu_id=onu_id)

                stdin.write(cmd)
                hasil = stdout.read().decode()
                print(hasil)
                result_list.append(hasil)
                message['result'] = hasil
                message['status'] = 'SUCCESS'
                send_log_detail_config_check(message)

            ssh_client.exec_command("logout")
            ssh_client.close()

        send_log_config_check(log_data)
        data = {
            "status": "SUCCESS",
            "data": data_list,
            "result": result_list
        }
        return Response(data)


class CheckViews(viewsets.ViewSet):

    def create(self, request):
        log_data = None
        status = 'SUCCESS'
        msg = 'NO ERROR'
        data_list = request.data
        result_list = []
        for data in data_list:
            ip_server = data['nms']['ip_server']
            port_tl1 = data['nms']['port_tl1']
            cmd_login = data['login'] if 'login' in data else None
            cmd_logout = data['logout'] if 'logout' in data else None
            cmd_list = data['cmd_list']['cmds']
            if log_data is None:
                log_data = get_log(request, 'CEK SN UNREG/REG/SERVICE')
            message = {
                'target': None,
                'action': data['cmd_list']['desc'],
                'status': 'IN_PROGRESS',
                'cmd': None,
                'result': None,
                'username': request.headers['Username'],
                'log_config_id': log_data['data']['id'],
            }
            try:
                config = TL1Socket(ip_server=ip_server, port_tl1=port_tl1)

                if cmd_login is not None:
                    reacv = config.command(cmd_login)
                    if reacv['status'].__eq__('FAILED'):
                        result = {
                            "command": 'LOGIN:::CTAG::UN=******,PWD=******;',
                            "result": reacv['result'],
                        }

                        cmd_split = cmd.split(',')
                        target = f"IP GPON: {cmd_split[0].split('::')[1].split('=')[1]}"
                        if len(cmd_split) > 1:
                            slot_port = cmd_split[1].split('=')[1].split('-')
                            slot = slot_port[2]
                            port = slot_port[3].split(":::;")
                            target += f", SLOT: {slot}, PORT: {port[0]}"

                        if len(cmd_split) > 2:
                            onu_id = cmd_split[3].split('=')[1].split(':::;')[0]
                            target += f", SN: {onu_id}"

                        message['status'] = reacv['status']
                        message['target'] = target
                        message['cmd'] = result['command']
                        message['result'] = reacv['result']

                        send_log_detail_config_check(message)
                        result_list.append(result)
                        config.close()
                        send_log_config_check(log_data)
                        data_json = {
                            "status": reacv['status'],
                            "message": reacv['message'],
                            "data": data_list,
                            "result": result_list
                        }
                        return Response(data_json)

                for cmd in cmd_list:
                    reacv = config.command(cmd)
                    result = {
                        "command": reacv['command'],
                        "result": reacv['result'],
                    }

                    cmd_split = cmd.split(',')
                    target = f"IP GPON: {cmd_split[0].split('::')[1].split('=')[1]}"
                    if len(cmd_split) > 1:
                        slot_port = cmd_split[1].split('=')[1].split('-')
                        slot = slot_port[2]
                        port = slot_port[3].split(":::;")
                        target += f", SLOT: {slot}, PORT: {port[0]}"

                    if len(cmd_split) > 2:
                        onu_id = cmd_split[3].split('=')[1].split(':::;')[0]
                        target += f", SN: {onu_id}"

                    message['status'] = reacv['status']
                    message['target'] = target
                    message['cmd'] = cmd
                    message['result'] = reacv['result']

                    send_log_detail_config_check(message)
                    result_list.append(result)

                    if reacv['status'].__eq__('FAILED'):
                        if cmd_logout is not None:
                            config.command(cmd_logout)

                        config.close()
                        send_log_config_check(log_data, 'FAILED')
                        data_json = {
                            "status": reacv['status'],
                            "message": reacv['message'],
                            "data": data_list,
                            "result": result_list
                        }
                        return Response(data_json)

                if cmd_logout is not None:
                    config.command(cmd_logout)

                config.close()
            except Exception as ex:
                status = 'FAILED'
                message['status'] = status
                message['target'] = f'IP SERVER: {ip_server}'
                message['cmd'] = f'telnet {ip_server}:{port_tl1}'
                message['result'] = ex.__str__()
                msg = message['result']
                print(msg)
                send_log_detail_config_check(message)

        send_log_config_check(log_data, status)
        data = {
            "status": status,
            "message": msg,
            "data": data_list,
            "result": result_list
        }
        return Response(data)


class CreateViews(viewsets.ViewSet):

    def create(self, request):
        data = request.data
        ip_server = data['nms']['ip_server']
        port_tl1 = data['nms']['port_tl1']
        cmd_login = data['login'] if 'login' in data else None
        cmd_logout = data['logout'] if 'logout' in data else None
        sts_cfg = data['status'] if 'status' in data else None
        ont = data['ont'] if 'ont' in data else None
        inet = data['inet'] if 'inet' in data else None
        voip = data['voip'] if 'voip' in data else None
        iptv = data['iptv'] if 'iptv' in data else None
        other = data['other'] if 'other' in data else None

        cmd_ont = None
        if ont is not None:
            cmd_ont = ont['cmd'] if 'cmd' in ont else None

        cmd_inet = None
        if inet is not None:
            cmd_inet = inet['cmds'] if 'cmds' in inet else None

        cmd_voip = None
        if voip is not None:
            cmd_voip = voip['cmds'] if 'cmds' in voip else None

        cmd_iptv = None
        if iptv is not None:
            cmd_iptv = iptv['cmds'] if 'cmds' in iptv else None

        cmd_other = None
        if other is not None:
            cmd_other = other['cmds'] if 'cmds' in other else None

        result_list = []
        status = 'SUCCESS'
        msg = 'NO ERROR'
        log_data = get_log(request, 'CONFIG', f"IP NMS: {ip_server}")
        message = {
            'target': None,
            'action': 'Connect to Server',
            'status': 'IN_PROGRESS',
            'cmd': None,
            'result': None,
            'username': request.headers['Username'],
            'log_config_id': log_data['data']['id'],
        }
        try:
            config = TL1Socket(ip_server=ip_server, port_tl1=port_tl1)

            if cmd_login is not None:
                reacv = config.command(cmd_login)
                if reacv['status'].__eq__('FAILED'):
                    message['action'] = data['ont']['desc']
                    result = {
                        "command": 'LOGIN:::CTAG::UN=******,PWD=******;',
                        "result": reacv['result'],
                    }
                    message['status'] = reacv['status']
                    get_log_detail(message, data['ont']['target'], result)
                    result_list.append(result)
                    config.close()
                    send_log_config_check(log_data, 'FAILED')
                    data_json = {
                        "status": reacv['status'],
                        "message": reacv['message'],
                        "data": data,
                        "result": result_list
                    }
                    return Response(data_json)

            i = 1
            if cmd_ont is not None and sts_cfg == 1:
                reacv = config.command(cmd_ont)
                message['action'] = data['ont']['desc']
                result = {
                    "command": reacv['command'],
                    "result": reacv['result'],
                }
                message['status'] = reacv['status']
                get_log_detail(message, data['ont']['target'], result)
                result_list.append(result)
                i += i
                if reacv['status'].__eq__('FAILED'):
                    if cmd_logout is not None:
                        config.command(cmd_logout)

                    config.close()
                    send_log_config_check(log_data, 'FAILED')
                    data_json = {
                        "status": reacv['status'],
                        "message": reacv['message'],
                        "data": data,
                        "result": result_list
                    }
                    return Response(data_json)

            i = 1
            if cmd_inet is not None:
                message['action'] = data['inet']['desc']
                for cmd in cmd_inet:
                    reacv = config.command(cmd)
                    result = {
                        "command": reacv['command'],
                        "result": reacv['result'],
                    }
                    message['status'] = reacv['status']
                    get_log_detail(message, data['inet']['target'], result)
                    result_list.append(result)
                    i += 1
                    if reacv['status'].__eq__('FAILED'):
                        if cmd_logout is not None:
                            config.command(cmd_logout)

                        config.close()
                        send_log_config_check(log_data, 'FAILED')
                        data_json = {
                            "status": reacv['status'],
                            "message": reacv['message'],
                            "data": data,
                            "result": result_list
                        }
                        return Response(data_json)

            i = 1
            if cmd_voip is not None:
                message['action'] = data['voip']['desc']
                for cmd in cmd_voip:
                    reacv = config.command(cmd)
                    result = {
                        "command": reacv['command'],
                        "result": reacv['result'],
                    }
                    message['status'] = reacv['status']
                    get_log_detail(message, data['voip']['target'], result)
                    result_list.append(result)
                    i += 1
                    if reacv['status'].__eq__('FAILED'):
                        if cmd_logout is not None:
                            config.command(cmd_logout)

                        config.close()
                        send_log_config_check(log_data, 'FAILED')
                        data_json = {
                            "status": reacv['status'],
                            "message": reacv['message'],
                            "data": data,
                            "result": result_list
                        }
                        return Response(data_json)

            i = 1
            if cmd_iptv is not None:
                message['action'] = data['iptv']['desc']
                for cmd in cmd_iptv:
                    reacv = config.command(cmd)
                    result = {
                        "command": reacv['command'],
                        "result": reacv['result'],
                    }
                    message['status'] = reacv['status']
                    get_log_detail(message, data['iptv']['target'], result)
                    result_list.append(result)
                    i += 1
                    if reacv['status'].__eq__('FAILED'):
                        if cmd_logout is not None:
                            config.command(cmd_logout)

                        config.close()
                        send_log_config_check(log_data, 'FAILED')
                        data_json = {
                            "status": reacv['status'],
                            "message": reacv['message'],
                            "data": data,
                            "result": result_list
                        }
                        return Response(data_json)

            i = 1
            if cmd_other is not None:
                message['action'] = data['other']['desc']
                for cmd in cmd_other:
                    reacv = config.command(cmd)
                    result = {
                        "command": reacv['command'],
                        "result": reacv['result'],
                    }
                    message['status'] = reacv['status']
                    get_log_detail(message, data['other']['target'], result)
                    result_list.append(result)
                    i += 1
                    if reacv['status'].__eq__('FAILED'):
                        if cmd_logout is not None:
                            config.command(cmd_logout)

                        config.close()
                        send_log_config_check(log_data, 'FAILED')
                        data_json = {
                            "status": reacv['status'],
                            "message": reacv['message'],
                            "data": data,
                            "result": result_list
                        }
                        return Response(data_json)

            i = 1
            if cmd_ont is not None and sts_cfg == 2:
                reacv = config.command(cmd_ont)
                message['action'] = data['ont']['desc']
                result = {
                    "command": reacv['command'],
                    "result": reacv['result'],
                }
                message['status'] = reacv['status']
                get_log_detail(message, data['ont']['target'], result)
                result_list.append(result)
                i += i
                if reacv['status'].__eq__('FAILED'):
                    if cmd_logout is not None:
                        config.command(cmd_logout)

                    config.close()
                    send_log_config_check(log_data, 'FAILED')
                    data_json = {
                        "status": reacv['status'],
                        "message": reacv['message'],
                        "data": data,
                        "result": result_list
                    }
                    return Response(data_json)

            if cmd_logout is not None:
                config.command(cmd_logout)

            config.close()

        except Exception as ex:
            status = 'FAILED'
            message['status'] = status
            message['target'] = f'IP SERVER: {ip_server}'
            message['cmd'] = f'telnet {ip_server}:{port_tl1}'
            message['result'] = ex.__str__()
            msg = message['result']
            print(msg)
            send_log_detail_config_check(message)
            result_list = result_list if len(result_list) > 0 else None

        send_log_config_check(log_data, status)

        data_json = {
            "status": status,
            "message": msg,
            "data": data,
            "result": result_list
        }
        return Response(data_json)


class BatchViews(viewsets.ViewSet):

    def create(self, request):
        datas = request.data
        result_list = []

        status = 'SUCCESS'
        msg = 'NO ERROR'
        log_data = get_log(request, 'CONFIG', None, datas['proccess_id'])
        message = {
            'target': None,
            'action': 'Connect to Server',
            'status': 'IN_PROGRESS',
            'cmd': None,
            'result': None,
            'username': request.headers['Username'],
            'log_config_id': log_data['data']['id'],
        }

        try:
            for data in datas['configs']:
                ip_server = data['nms']['ip_server']
                port_tl1 = data['nms']['port_tl1']
                cmd_login = data['login'] if 'login' in data else None
                cmd_logout = data['logout'] if 'logout' in data else None
                details = data['details']

                config = TL1Socket(ip_server=ip_server, port_tl1=port_tl1)

                if cmd_login is not None:
                    reacv = config.command(cmd_login)
                    if reacv['status'].__eq__('FAILED'):
                        message['action'] = data['ont']['desc']
                        result = {
                            "command": 'LOGIN:::CTAG::UN=******,PWD=******;',
                            "result": reacv['result'],
                        }
                        message['status'] = reacv['status']
                        get_log_detail(message, data['ont']['target'], result)
                        result_list.append(result)
                        config.close()
                        send_log_config_check(log_data, 'FAILED')
                        data_json = {
                            "status": reacv['status'],
                            "message": reacv['message'],
                            "data": data,
                            "result": result_list
                        }
                        return Response(data_json)

                for detail in details:
                    sts_cfg = detail['status'] if 'status' in detail else None
                    ont = detail['ont'] if 'ont' in detail else None
                    inet = detail['inet'] if 'inet' in detail else None
                    voip = detail['voip'] if 'voip' in detail else None
                    iptv = detail['iptv'] if 'iptv' in detail else None
                    other = detail['other'] if 'other' in detail else None

                    cmd_ont = None
                    if ont is not None:
                        cmd_ont = ont['cmd'] if 'cmd' in ont else None

                    cmd_inet = None
                    if inet is not None:
                        cmd_inet = inet['cmds'] if 'cmds' in inet else None

                    cmd_voip = None
                    if voip is not None:
                        cmd_voip = voip['cmds'] if 'cmds' in voip else None

                    cmd_iptv = None
                    if iptv is not None:
                        cmd_iptv = iptv['cmds'] if 'cmds' in iptv else None

                    cmd_other = None
                    if other is not None:
                        cmd_other = other['cmds'] if 'cmds' in other else None

                    i = 1
                    if cmd_ont is not None and sts_cfg == 1:
                        reacv = config.command(cmd_ont)
                        message['action'] = detail['ont']['desc']
                        result = {
                            "command": reacv['command'],
                            "result": reacv['result'],
                        }
                        message['status'] = reacv['status']
                        get_log_detail(message, detail['ont']['target'], result)
                        result_list.append(result)
                        i += i
                        if reacv['status'].__eq__('FAILED'):
                            if 'ADD-ONU' in cmd_ont:
                                if cmd_logout is not None:
                                    config.command(cmd_logout)

                                config.close()
                                send_log_config_check(log_data, 'FAILED')
                                data_json = {
                                    "status": reacv['status'],
                                    "message": reacv['message'],
                                    "data": detail,
                                    "result": result_list
                                }
                                return Response(data_json)

                    i = 1
                    if cmd_inet is not None:
                        message['action'] = detail['inet']['desc']
                        for cmd in cmd_inet:
                            reacv = config.command(cmd)
                            result = {
                                "command": reacv['command'],
                                "result": reacv['result'],
                            }
                            message['status'] = reacv['status']
                            get_log_detail(message, detail['inet']['target'], result)
                            result_list.append(result)
                            i += 1
                            if reacv['status'].__eq__('FAILED'):
                                if cmd_logout is not None:
                                    config.command(cmd_logout)

                                config.close()
                                send_log_config_check(log_data, 'FAILED')
                                data_json = {
                                    "status": reacv['status'],
                                    "message": reacv['message'],
                                    "data": detail,
                                    "result": result_list
                                }
                                return Response(data_json)

                    i = 1
                    if cmd_voip is not None:
                        message['action'] = detail['voip']['desc']
                        for cmd in cmd_voip:
                            reacv = config.command(cmd)
                            result = {
                                "command": reacv['command'],
                                "result": reacv['result'],
                            }
                            message['status'] = reacv['status']
                            get_log_detail(message, detail['voip']['target'], result)
                            result_list.append(result)
                            i += 1
                            if reacv['status'].__eq__('FAILED'):
                                if cmd_logout is not None:
                                    config.command(cmd_logout)

                                config.close()
                                send_log_config_check(log_data, 'FAILED')
                                data_json = {
                                    "status": reacv['status'],
                                    "message": reacv['message'],
                                    "data": detail,
                                    "result": result_list
                                }
                                return Response(data_json)

                    i = 1
                    if cmd_iptv is not None:
                        message['action'] = detail['iptv']['desc']
                        for cmd in cmd_iptv:
                            reacv = config.command(cmd)
                            result = {
                                "command": reacv['command'],
                                "result": reacv['result'],
                            }
                            message['status'] = reacv['status']
                            get_log_detail(message, detail['iptv']['target'], result)
                            result_list.append(result)
                            i += 1
                            if reacv['status'].__eq__('FAILED'):
                                if cmd_logout is not None:
                                    config.command(cmd_logout)

                                config.close()
                                send_log_config_check(log_data, 'FAILED')
                                data_json = {
                                    "status": reacv['status'],
                                    "message": reacv['message'],
                                    "data": detail,
                                    "result": result_list
                                }
                                return Response(data_json)

                    i = 1
                    if cmd_other is not None:
                        message['action'] = detail['other']['desc']
                        for cmd in cmd_other:
                            reacv = config.command(cmd)
                            result = {
                                "command": reacv['command'],
                                "result": reacv['result'],
                            }
                            message['status'] = reacv['status']
                            get_log_detail(message, detail['other']['target'], result)
                            result_list.append(result)
                            i += 1
                            if reacv['status'].__eq__('FAILED'):
                                if cmd_logout is not None:
                                    config.command(cmd_logout)

                                config.close()
                                send_log_config_check(log_data, 'FAILED')
                                data_json = {
                                    "status": reacv['status'],
                                    "message": reacv['message'],
                                    "data": detail,
                                    "result": result_list
                                }
                                return Response(data_json)

                    i = 1
                    if cmd_ont is not None and sts_cfg == 2:
                        reacv = config.command(cmd_ont)
                        message['action'] = detail['ont']['desc']
                        result = {
                            "command": reacv['command'],
                            "result": reacv['result'],
                        }
                        message['status'] = reacv['status']
                        get_log_detail(message, detail['ont']['target'], result)
                        result_list.append(result)
                        i += i
                        if reacv['status'].__eq__('FAILED'):
                            if 'ADD-ONU' in cmd_ont:
                                if cmd_logout is not None:
                                    config.command(cmd_logout)

                                config.close()
                                send_log_config_check(log_data, 'FAILED')
                                data_json = {
                                    "status": reacv['status'],
                                    "message": reacv['message'],
                                    "data": detail,
                                    "result": result_list
                                }
                                return Response(data_json)

                if cmd_logout is not None:
                    config.command(cmd_logout)

                config.close()

        except Exception as ex:
            status = 'FAILED'
            message['status'] = status
            message['target'] = f'IP SERVER: {ip_server}'
            message['cmd'] = f'telnet {ip_server}:{port_tl1}'
            message['result'] = ex.__str__()
            msg = message['result']
            print(msg)
            send_log_detail_config_check(message)
            result_list = result_list if len(result_list) > 0 else None

        send_log_config_check(log_data, status)

        data_json = {
            "status": status,
            "message": msg,
            "data": data,
            "result": result_list
        }
        return Response(data_json)


@api_view()
def info(request):
    now = datetime.now()
    dt_obj = datetime.strptime(now.strftime("%d.%m.%Y %H:%M:%S,%f"), "%d.%m.%Y %H:%M:%S,%f")
    microsec = dt_obj.timestamp() * 1000000
    return Response(
        {
            "status": "SUCCESS",
            "message": f"Berhasil Konek Ke Service, nilai: {microsec}"
        }
    )
