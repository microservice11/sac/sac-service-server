FROM microservices:python
ENV PYTHONUNBUFFERED=1
RUN mkdir /opt/sac-service-server
COPY . /opt/sac-service-server
WORKDIR /opt/sac-service-server
RUN pip install --no-cache-dir -r requirements.txt
EXPOSE 8002
ENTRYPOINT ["gunicorn", "sac-service-server.wsgi:application", "--bind", ":8002", "--worker-class", "gevent", "--log-level", "debug", "--workers", "3"]
